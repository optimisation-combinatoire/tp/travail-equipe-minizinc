%include "bench_three_schedule.dzn";
include "arg_max.mzn";
%%%%%%%%%%%%%%%%%%%%%%
% Constants params   %
%%%%%%%%%%%%%%%%%%%%%%
int: OPENING_INDEX;
int: CLOSING_INDEX;
int: NB_PERIODS;
int: NB_EMPLOYEES;
int: NB_ACTIVITES;
set of int: INDEX_OF_LUNCH_TIME = OPENING_INDEX..CLOSING_INDEX;
set of int: PERIODS = 1..NB_PERIODS;
set of int: ACTIVITIES = 1..NB_ACTIVITES;
set of int: EMPLOYEES = 1.. NB_EMPLOYEES;
set of int: EMPLOYEE_STATE = 0..NB_ACTIVITES;
array[PERIODS, ACTIVITIES] of EMPLOYEE_STATE: DEMAND;
array[PERIODS, ACTIVITIES] of int: OVER_COST;
array[PERIODS, ACTIVITIES] of int: UNDER_COST;
%%%%%%%%%%%%%%%%%%%%%%
%    Assertion       %
%%%%%%%%%%%%%%%%%%%%%%
constraint assert(OPENING_INDEX >=0/\OPENING_INDEX<CLOSING_INDEX,"Invalid datafile: " ++
                  "Opening index is negative or greater then closing index "++"\(OPENING_INDEX)");
                  
constraint assert(CLOSING_INDEX <=NB_PERIODS/\CLOSING_INDEX>OPENING_INDEX,"Invalid datafile: " ++
                  "closing index is greater than the number of period or lower then opening index "++"\(CLOSING_INDEX)");
%%%%%%%%%%%%%%%%%%%%%%
% Decision variables %
%%%%%%%%%%%%%%%%%%%%%%
var int: profit_lost; % Minimize this loss
array [PERIODS, EMPLOYEES] of var EMPLOYEE_STATE: schedule; % Output this schedule
array [EMPLOYEES] of var INDEX_OF_LUNCH_TIME:index_of_lunch; % Index of the lunch of every worker
array [EMPLOYEES] of var OPENING_INDEX..CLOSING_INDEX:work_start_index; % the hour the worker start
array [EMPLOYEES] of var OPENING_INDEX..CLOSING_INDEX:work_end_index; % the hour the worker start
%%%%%%%%%%%%%%%%%%%%%%
%    Constraints     %
%%%%%%%%%%%%%%%%%%%%%%
% calculate total loss - sums all losses for 96 periods
profit_lost = sum(p in PERIODS)(loss(row(DEMAND, p), row(schedule, p), row(OVER_COST, p), row(UNDER_COST,p), ACTIVITIES)); 
% Calculate index_of_lunch
constraint forall(e in EMPLOYEES)(index_of_lunch[e] = get_index_of_lunch(col(schedule,e)));
% Calculate the starting hour
constraint forall(e in EMPLOYEES)(work_start_index[e] = get_index_begining_work(col(schedule,e)));
% Calculate the ending hour
constraint forall(e in EMPLOYEES)(work_end_index[e] = get_index_ending_work(col(schedule,e)));
% Work should start when shop opens
constraint forall(e in EMPLOYEES)(work_start_index[e] >= OPENING_INDEX);
% Work should end when shop closes
constraint forall(e in EMPLOYEES)(work_end_index[e] <= CLOSING_INDEX);
% Total hours at actual task
constraint forall(e in EMPLOYEES)(work_time(work_start_index[e],work_end_index[e]));         
% Every non zero substring must be at least of lenght 4.
constraint forall(e in EMPLOYEES)(activity_duration_minimum_an_hour(col(schedule,e),work_start_index[e],work_end_index[e]));
% Worker can only change activity after a break or a lunch
constraint forall(e in EMPLOYEES)(can_only_change_activity_after_break(col(schedule,e),work_start_index[e],work_end_index[e]));
% There is a break then a lunch and a break
constraint forall(e in EMPLOYEES)(the_Sequence_break_lunch_break_is_respected(index_of_lunch[e],col(schedule,e),work_start_index[e],work_end_index[e]));
% can only have 6 break
constraint forall(e in EMPLOYEES)(can_only_have_six_break_period(work_start_index[e],work_end_index[e],col(schedule,e)));
% Solve
solve 
:: restart_linear(1) 
%:: int_search(schedule, most_constrained, indomain_split_random)
%:: int_search(work_start_index, most_constrained, indomain_min)
%:: int_search(work_end_index, most_constrained, indomain_max)

minimize profit_lost;
%%%%%%%%%%%%%%%%%%%%%%%%
%      Output          %
%%%%%%%%%%%%%%%%%%%%%%%%  
output ["Schedule of employees is as follows: \n\n"];
output [if p == NB_PERIODS then show(schedule[p, e]) ++ "\n\n" else show(schedule[p, e]) endif 
         |  e in 1..NB_EMPLOYEES, p in 1..NB_PERIODS];
output ["Loss is: \(profit_lost)\n\n"];
output ["index of lunch: \(index_of_lunch)\n\n"];
output ["index of first period at work: \(work_start_index)\n\n"];
output ["index of last period at work: \(work_end_index)"];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%      FUNCTIONS and PREDICATES         %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% calculate loss for each 15 minute period                
function var int: loss(array[ACTIVITIES] of EMPLOYEE_STATE: dem, array[EMPLOYEES] of var EMPLOYEE_STATE: sch, array[ACTIVITIES] of int: ovc, array[ACTIVITIES] of int: unc, set of int: Act) 
                = sum(a in Act)(if count(sch, a) > dem[a] then ovc[a]*(count(sch, a) - dem[a]) elseif count(sch, a) < dem[a] then unc[a]*(dem[a] - count(sch , a)) else 0 endif ); 
                
% Employee stays at word between 7 to 9 hours, does not enforce interval etc.                                
predicate work_time(var PERIODS :entry,var PERIODS :exit) = exit-entry+1>=28/\exit-entry+1<=36;
% Make sure employee work 1 hour in a stretch
predicate activity_duration_minimum_an_hour(array[PERIODS] of var EMPLOYEE_STATE:schedule,var OPENING_INDEX..CLOSING_INDEX :work_start_index,
var OPENING_INDEX..CLOSING_INDEX :work_end_index)=
forall(i in work_start_index..work_end_index)(
  % First three 
  if ((i==1 \/ i==2 \/ i==3)/\(schedule[i]!=0)) then
     (schedule[1]==schedule[2] /\ schedule[1]==schedule[3] /\ schedule[1]==schedule[4])
  % From 4 to 93 : [X]XXX, X[X]XX, XX[X]X, XXX[X]
  elseif i>=4/\i<=NB_PERIODS-3/\ (schedule[i]!=0) then
     (schedule[i]==schedule[i+1] /\ schedule[i]==schedule[i+2] /\ schedule[i]==schedule[i+3])
  \/ (schedule[i]==schedule[i-1] /\ schedule[i]==schedule[i+1] /\ schedule[i]==schedule[i+2])
  \/ (schedule[i]==schedule[i-2] /\ schedule[i]==schedule[i-1] /\ schedule[i]==schedule[i+1])
  \/ (schedule[i]==schedule[i-3] /\ schedule[i]==schedule[i-2] /\ schedule[i]==schedule[i-1])
  % Last three
  elseif ((i==NB_PERIODS-2 \/ i=NB_PERIODS-1 \/ i=NB_PERIODS)/\(schedule[i]!=0)) then
     (schedule[NB_PERIODS-3]==schedule[NB_PERIODS-2] /\ schedule[NB_PERIODS-3]==schedule[NB_PERIODS-1] /\ schedule[NB_PERIODS-3]==schedule[NB_PERIODS])
  else true
 endif);
 
% schedule at i and i+1 is not same and 0 value at i implies that i+1 has value 0
predicate can_only_change_activity_after_break(array[PERIODS] of var EMPLOYEE_STATE:schedule,var OPENING_INDEX..CLOSING_INDEX :work_start_index,
var OPENING_INDEX..CLOSING_INDEX :work_end_index) = 
 forall(i in work_start_index..work_end_index)((schedule[i]!=schedule[i+1] /\ schedule[i]!=0) -> schedule[i+1]==0);

% inspired of https://stackoverflow.com/questions/35758640/index-of-string-value-in-minizinc-array
% Detect 011110 pattern in 5..90
function var OPENING_INDEX..CLOSING_INDEX: get_index_of_lunch(array[PERIODS] of var EMPLOYEE_STATE:schedule) = 
   sum([if schedule[i-1]!=0 /\ schedule[i]==0 /\ schedule[i+1]==0 /\ schedule[i+2]==0 /\ schedule[i+3]==0 /\ schedule[i+4]!=0
         then i else 0 endif | i in OPENING_INDEX..CLOSING_INDEX]);

% inspired of https://stackoverflow.com/questions/35758640/index-of-string-value-in-minizinc-array  
% Detect 010 pattern in beginning..ending
predicate there_is_a_break_between(var INDEX_OF_LUNCH_TIME: beginning, var OPENING_INDEX..CLOSING_INDEX : ending, array[PERIODS] of var EMPLOYEE_STATE:schedule) = 
   sum([if schedule[i-1]!=0 /\ schedule[i]==0 /\ schedule[i+1]!=0 then 1 else 0 endif | i in beginning..ending])==1;

% First break can be anywhere after index 4 and second break cab be anywhere after lunch
predicate the_Sequence_break_lunch_break_is_respected(var int:lunch_index, array[PERIODS] of var EMPLOYEE_STATE:schedule,var OPENING_INDEX..CLOSING_INDEX :work_start_index,
var OPENING_INDEX..CLOSING_INDEX :work_end_index) =
   there_is_a_break_between(work_start_index,lunch_index-4,schedule) /\ there_is_a_break_between(lunch_index+1,work_end_index,schedule);
   
function var OPENING_INDEX..CLOSING_INDEX : get_index_begining_work(array[PERIODS] of var EMPLOYEE_STATE:schedule)=
  arg_max([if schedule[i]!=0 then 1 else 0 endif |i in 1..CLOSING_INDEX]);

function var OPENING_INDEX..CLOSING_INDEX : get_index_ending_work(array[PERIODS] of var EMPLOYEE_STATE:schedule)=
  NB_PERIODS-arg_max(reverse([if el!=0 then 1 else 0 endif |el in schedule]))+1;

% inspired of https://stackoverflow.com/questions/35758640/index-of-string-value-in-minizinc-array
predicate can_only_have_six_break_period(var PERIODS:start,var PERIODS:end,array[PERIODS] of var EMPLOYEE_STATE:schedule) = 
  sum([if schedule[i]==0 then 1 else 0 endif|i in start..end ])==6;